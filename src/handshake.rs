use crate::transmission_handler::TransmissionHandler;

struct ActOne([u8; 50]);

struct ActTwo([u8; 50]);

struct ActThree([u8; 50]);

enum Act {
    One(ActOne),
    Two(ActTwo),
    Three(ActThree),
}

impl Act {
    pub fn serialize(&self) -> Vec<u8> {
        match self {
            Act::One(act) => {
                act.0.to_vec()
            }
            Act::Two(act) => {
                act.0.to_vec()
            }
            Act::Three(act) => {
                act.0.to_vec()
            }
        }
    }
}

pub struct Handshake {}

impl Handshake {
    pub fn new() -> Self {}
    pub fn initiate() -> ActOne {}
    pub fn from_data(data: &[u8]) -> (Self, Option<Act>) {
        // filter first 50 bytes
    }

    pub fn process(&self, data: &[u8]) -> (Option<Act>, Option<TransmissionHandler>) {
        // filter first 50 bytes
    }

    pub fn process_act_one(act: ActOne) -> ActTwo {
        unimplemented!()
    }

    pub fn process_act_two(act: ActTwo) -> (ActThree, TransmissionHandler) {
        unimplemented!()
    }

    pub fn process_act_tree(act: ActThree) -> TransmissionHandler {
        unimplemented!()
    }
}